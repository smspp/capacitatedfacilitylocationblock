Single Source Capacitated Facility Location Problem instances in netCDF SMS++ format

The instances have been obtained out of either the ORLib instances found in the
txt/ORLib folder or the instances found in the orig folder using the txt2nc4
converter available in the /tools folder. See the readme files in the original
folders for more information.